package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.List;

public class TCPServerThread extends Thread {
    public String getUsername() {
        return username;
    }

    private String username;
    private BufferedReader inputStream;
    private PrintStream outputStream;
    private Socket clientSocket;

    public InetAddress getAddress() {
        return address;
    }

    private InetAddress address;

    public int getPort() {
        return port;
    }

    private int port;
    private final List<TCPServerThread> clientThreads;

    public TCPServerThread(Socket clientSocket, InetAddress address, int port, List<TCPServerThread> clientThreads) {
        this.clientSocket = clientSocket;
        this.address = address;
        this.port = port;
        this.clientThreads = clientThreads;
    }

    public void run() {
        try {
            inputStream = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            outputStream = new PrintStream(clientSocket.getOutputStream());
            //Ask for a username
            outputStream.println("Enter username: ");
            username = inputStream.readLine().trim();
            //Say hello to our new client
            outputStream.println("Welcome to the chat. Type /exit to leave the chat.");

            for (TCPServerThread t : clientThreads) {
                if (t != this)
                    t.outputStream.println(username + " has entered the chat.");
            }


            //Conversation logic
            while (true) {
                String text = inputStream.readLine();
                if (text.contains("/exit"))
                    break;

                for (TCPServerThread t : clientThreads) {
                    if (t != this && t.username != null) {
                        t.outputStream.println(username + ": " + text);
                    }

                }
            }

            //Exiting the chat room

            for (TCPServerThread t : clientThreads) {
                if (t != this && t.username != null) {
                    t.outputStream.println(username + " has left the chat.");
                }
            }
            outputStream.println("You have been disconnected from the server.");
            clientThreads.remove(this);

            inputStream.close();
            outputStream.close();
            clientSocket.close();

        } catch (IOException e) {
            System.out.print(e);
        }
    }
}
