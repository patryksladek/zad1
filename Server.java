package com.company;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Server {
    //TCP
    private static ServerSocket serverSocket = null;
    //UDP
    private static DatagramSocket udpSocket = null;

    private static List<TCPServerThread> clientThreads = Collections.synchronizedList(new ArrayList<>());


    public static void main(String args[]) {
        int port = 8000;
        if (args.length > 1) {
            port = Integer.valueOf(args[0]);
        }
        System.out.println("You are using port " + port);

        //Opening server socket
        try {
            serverSocket = new ServerSocket(port);
            udpSocket = new DatagramSocket(port);
        } catch (IOException e) {
            System.out.println(e);
        }

        UDPServerThread d = new UDPServerThread(clientThreads, udpSocket);
        d.start();
        //Handling clients connections
        while (true) {
            try {
                Socket clientSocket = serverSocket.accept();
                TCPServerThread t = new TCPServerThread(clientSocket, clientSocket.getInetAddress(),
                        clientSocket.getPort(), clientThreads);
                clientThreads.add(t);
                t.start();
            } catch (IOException e) {
                System.out.println(e);
            }
        }

    }
}
