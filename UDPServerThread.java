package com.company;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.List;

public class UDPServerThread extends Thread {

    private final DatagramSocket udpSocket;
    private final List<TCPServerThread> clientThreads;

    public UDPServerThread(List<TCPServerThread> clientThreads, DatagramSocket udpSocket) {
        this.clientThreads = clientThreads;
        this.udpSocket = udpSocket;

    }

    public void run() {
        byte[] receiveBuffer = new byte[1024];
        //Get the data from socket
        while (true) {

            try {
                DatagramPacket receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
                udpSocket.receive(receivePacket);
                String username = GetSenderName(receivePacket.getAddress(), receivePacket.getPort());
                StringBuilder sb = new StringBuilder();
                sb.append(username).append(":\n");
                sb.append(new String(receivePacket.getData()));

                for (TCPServerThread t : clientThreads) {
                    InetAddress address = t.getAddress();
                    int port = t.getPort();
                    if (t.getUsername() != username) {
                        byte[] sendData = sb.toString().getBytes();
                        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, address, port);
                        udpSocket.send(sendPacket);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private String GetSenderName(InetAddress adr, int port) {
        for (TCPServerThread t : clientThreads) {
            if (t.getAddress().equals(adr) && t.getPort() == port)
                return t.getUsername();
        }
        return "";
    }
}
