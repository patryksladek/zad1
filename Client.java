package com.company;

import java.io.*;
import java.net.*;
import java.util.Objects;

/**
 * Created by sladz on 09.03.2017.
 */
public class Client implements Runnable {
    //Client sockets
    private static Socket tcpSocket;
    private static MulticastSocket multicastSocket;
    private static DatagramSocket udpSocket;
    //Streams
    private static PrintStream outputStream;
    private static BufferedReader inputStream;

    private static BufferedReader consoleInputReader;
    private static boolean isClosed = false;

    private static InetAddress address;

    private static InetAddress group;

    public static void main(String[] args) {
        int port;
        int localPort;
        String serverAddress, clientAddress;
        if (args.length < 4) {
            System.out.println("Usage: <serverAddress> <serverPort> <clientAddress> <clientPort>");
            return;
        } else {
            serverAddress = args[0];
            port = Integer.valueOf(args[1]);
            clientAddress = args[2];
            localPort = Integer.valueOf(args[3]);


        }
        System.out.println("Connecting to host: " + serverAddress + " on port number: " + port);

        int multicastPort = 9001;
        try {
            address = InetAddress.getByName(serverAddress);
            tcpSocket = new Socket(serverAddress, port, InetAddress.getByName(clientAddress), localPort);
            udpSocket = new DatagramSocket(localPort);
            multicastSocket = new MulticastSocket(multicastPort);

            group = InetAddress.getByName("224.0.01");
            multicastSocket.joinGroup(group);

            consoleInputReader = new BufferedReader(new InputStreamReader(System.in));
            outputStream = new PrintStream(tcpSocket.getOutputStream());
            inputStream = new BufferedReader(new InputStreamReader(tcpSocket.getInputStream()));
            address = InetAddress.getByName(serverAddress);
        } catch (IOException e) {
            System.out.println(e);
        }

        if (tcpSocket != null && outputStream != null && inputStream != null && udpSocket != null && multicastSocket != null) {
            try {
                new Thread(new Client()).start();
                new Thread(new ClientUDPHandler(udpSocket)).start();
                new Thread(new ClientMulticastHandler(multicastSocket, group, multicastPort)).start();
                String msg = null;
                //Read username
                msg = consoleInputReader.readLine().trim();
                String username = msg;
                outputStream.println(msg);
                //Send the data to the server
                while (!isClosed) {
                    msg = consoleInputReader.readLine().trim();
                    byte[] sendBuffer;
                    if (msg.equals("M")) {
                        sendBuffer = "HELLO FROM UDP".getBytes();
                        DatagramPacket sendPacket = new DatagramPacket(sendBuffer, sendBuffer.length, address, port);
                        udpSocket.send(sendPacket);
                    } else if (msg.equals("N")) {
                        sendBuffer = (username+":\n"+"HELLO FROM MULTICAST").getBytes();
                        DatagramPacket sendPacket = new DatagramPacket(sendBuffer, sendBuffer.length, group, multicastPort);
                        multicastSocket.send(sendPacket);
                    } else {
                        outputStream.println(msg);
                    }

                }
                //Closing client
                outputStream.close();
                inputStream.close();
                tcpSocket.close();
                udpSocket.close();
                multicastSocket.close();
            } catch (IOException e) {
                System.out.println(e);
            }
        }
    }

    @Override
    public void run() {
        String response;
        try {
            while ((response = inputStream.readLine()) != null) {
                System.out.println(response);
                if (response.contains("You have been disconnected from the server."))
                    break;
            }
            isClosed = true;
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
