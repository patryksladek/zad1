package com.company;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

/**
 * Created by sladz on 11.03.2017.
 */
public class ClientMulticastHandler extends Thread {

    private final MulticastSocket multicastSocket;
    private final InetAddress group;
    private final int localPort;

    public ClientMulticastHandler(MulticastSocket multicastSocket, InetAddress group, int localPort) {
        this.multicastSocket = multicastSocket;
        this.group = group;
        this.localPort = localPort;
    }

    public void run()
    {
        while(true){
            try {
                byte[] buf = new byte[1000];
                DatagramPacket recv = new DatagramPacket(buf, buf.length);
                multicastSocket.receive(recv);
                System.out.println(new  String(recv.getData()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
