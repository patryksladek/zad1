package com.company;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * Created by sladz on 09.03.2017.
 */
public class ClientUDPHandler implements Runnable {
    private DatagramSocket udpSocket;
    public ClientUDPHandler(DatagramSocket udpSocket) {
        this.udpSocket = udpSocket;
    }

    @Override
    public void run() {
        try{
            byte[] receiveBuffer = new byte[1024];
            while(true){
                DatagramPacket receivePacket = new DatagramPacket(receiveBuffer,receiveBuffer.length);
                udpSocket.receive(receivePacket);
                String msg = new String(receivePacket.getData());
                System.out.println(msg);
            }
        } catch (IOException e) {
            System.out.println(e);
        }
        finally{
            if(udpSocket!=null)
                udpSocket.close();
        }

    }
}
